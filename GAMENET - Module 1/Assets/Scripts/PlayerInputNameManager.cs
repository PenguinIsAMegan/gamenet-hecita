﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerInputNameManager : MonoBehaviour
{
   public void SetPlayerName(string playerName)
    {
        if (string.IsNullOrEmpty(playerName))
        {
            // If playerName is empty, terminates the whole method 
            Debug.Log("Player name is empty!");
            return;
        }

        // Sets name of player in network
        PhotonNetwork.NickName = playerName;
    }
}
