﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public GameObject playerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        Transform point = Spawner.Instance.GetSpawnPoint();

        if (PhotonNetwork.IsConnectedAndReady)
            PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(point.position.x, 0, point.position.z), Quaternion.identity);
    }
}
