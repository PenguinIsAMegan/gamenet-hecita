﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    private int killCount = 0;
    public int killCountWinCondition;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
        killCountWinCondition = 10;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
                GameObject target = hit.collider.gameObject;

                if (target.GetComponent<Shooting>().health <= 0)
                {
                    this.killCount++;

                    Debug.Log(this.photonView.Owner.NickName + " kills " + this.killCount);

                    if (this.killCount >= killCountWinCondition)
                        this.GetComponent<PhotonView>().RPC("TriggerAnnounceWinner", RpcTarget.AllBuffered, this.photonView.Owner.NickName);
                }
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die();
            this.GetComponent<PhotonView>().RPC("TriggerAnnounceDeath", RpcTarget.AllBuffered, info.Sender.NickName, info.photonView.Owner.NickName);
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);

           StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("RespawnText");
        GameObject deathText = GameObject.Find("DeathTextUI");

        float respawnTime = 5.0f;

        while (respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";
        deathText.GetComponent<Text>().text = "";

        Transform point = Spawner.Instance.GetSpawnPoint();

        this.transform.position = new Vector3(point.position.x, 0, point.position.z);
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RegainHealth()
    {
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void TriggerAnnounceDeath(string killer, string killed)
    {
       StartCoroutine(AnnouncePlayerDeath(killer, killed));
    }

    IEnumerator AnnouncePlayerDeath(string killer, string killed)
    {
        GameObject deathText = GameObject.Find("DeathTextUI");
        deathText.GetComponent<Text>().text = killer + " killed " + killed;

        yield return new WaitForSeconds(3f);

        deathText.GetComponent<Text>().text = "";
    }

    [PunRPC]
    public void TriggerAnnounceWinner(string winner)
    {
        Spawner.Instance.ActivateWinScreen();

        StartCoroutine(AnnounceWinner(winner));
    }

    IEnumerator AnnounceWinner(string winner)
    {
        GameObject respawnText = GameObject.Find("WinText");
        respawnText.GetComponent<Text>().text = winner + " won!";

        yield return new WaitForSeconds(3f);

        respawnText.GetComponent<Text>().text = "";

        PhotonNetwork.LoadLevel("LobbyScene");

        PhotonNetwork.AutomaticallySyncScene = true;
    }
}
