﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : Singleton<Spawner>
{
    public new static Spawner Instance;

    public GameObject winScreen;

    [SerializeField] Transform[] spawnPoints;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
            Instance = this;

        winScreen.SetActive(false);
    }

    public Transform GetSpawnPoint()
    {
        int num = Random.Range(0, 3);

        return spawnPoints[num];
    }  

    public void ActivateWinScreen()
    {
        winScreen.SetActive(true);
    }
}
