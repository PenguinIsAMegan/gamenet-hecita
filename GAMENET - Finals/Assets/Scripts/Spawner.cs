﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Spawner : MonoBehaviourPunCallbacks
{
    public GameObject[] enemyStartingPositions;
    public Buff[] buffArray;

    public GameObject enemySlimePrefab;
    public GameObject boundingBox;
    public bool canSpawn = false;

    MeshCollider spawnArea;

    public static Spawner Instance = null;

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)EventCodes.RaiseEventCode.SpawnPickUpEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            Vector2 pos = (Vector2)data[0];

            int randNum = (int)data[1];

            if (canSpawn)
            {
                Buff prefab = buffArray[randNum];

                Instantiate(prefab, pos, Quaternion.identity);
            }
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        spawnArea = boundingBox.GetComponent<MeshCollider>();

        if (PhotonNetwork.IsMasterClient)
        {
            canSpawn = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnSlime()
    {
        if (canSpawn)
        {
            int num = Random.Range(0, 3);
            GameObject pos = enemyStartingPositions[num];
            PhotonNetwork.Instantiate(enemySlimePrefab.name, pos.transform.position, Quaternion.identity);

            StartCoroutine(SpawnMoreSlimes());
        }
    }

    IEnumerator SpawnMoreSlimes()
    {
        yield return new WaitForSeconds(3.0f);

        SpawnSlime();
    }

    public void SpawnPickUp(Vector2 enemyPos)
    {
        if (canSpawn)
        {
            Vector2 pos = enemyPos;

            int randNum = Random.Range(0, buffArray.Length);

            object[] data = new object[] { pos, randNum };

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOption = new SendOptions
            {
                Reliability = false
            };

            PhotonNetwork.RaiseEvent((byte)EventCodes.RaiseEventCode.SpawnPickUpEventCode, data, raiseEventOptions, sendOption);
        }
    }
}
