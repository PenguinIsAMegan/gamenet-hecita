﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerInput : MonoBehaviourPunCallbacks
{
    private float speed = 5;
    private Animator animator;
    
    public GameObject playerHealthAndNameUI;

    [Header("Projectile")]
    public GameObject projectilePoint;
    public GameObject projectilePrefab;

// Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            AssignDirection();
        }
    }

    void LateUpdate()
    {
        Movement();
    }

    void Movement()
    {
        float hMove = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        this.transform.position += new Vector3(hMove, 0, 0);

        float vMove = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        this.transform.position += new Vector3(0, vMove, 0);

        RenderAnimation(hMove, vMove);
    }

    void RenderAnimation(float hMove, float vMove)
    {
        if (Mathf.Abs(hMove) > 0 || Mathf.Abs(vMove) > 0)
        {
            animator.SetBool("isWalking", true);
        }

        else
        {
            animator.SetBool("isWalking", false);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        else if (Input.GetKeyDown(KeyCode.A))
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0); 
        }

        photonView.RPC("fixPlayerNameAndHealthUIRotation", RpcTarget.All);
    }

    [PunRPC]
    public void fixPlayerNameAndHealthUIRotation()
    {
        playerHealthAndNameUI.transform.rotation = Quaternion.Euler(0, 0, 0);
    }


    void AssignDirection()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            photonView.RPC("Shoot", RpcTarget.AllBuffered, Quaternion.Euler(0, 0, 0), Vector3.right);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            photonView.RPC("Shoot", RpcTarget.AllBuffered, Quaternion.Euler(0, 0, 90), Vector3.up);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            photonView.RPC("Shoot", RpcTarget.AllBuffered, Quaternion.Euler(0, 0, 180), Vector3.left);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            photonView.RPC("Shoot", RpcTarget.AllBuffered, Quaternion.Euler(0, 0, 270), Vector3.down);
        }
    }

    [PunRPC]
    void Shoot(Quaternion rotation, Vector3 direction)
    {
        GameObject projectile = Instantiate(projectilePrefab, projectilePoint.transform.position, rotation);
        projectile.GetComponent<Projectile>().direction = direction;
        projectile.GetComponent<Projectile>().owner = this.gameObject;
    }

    public void ChangeSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    public void ResetSpeed()
    {
        speed = 5;
    }
}
