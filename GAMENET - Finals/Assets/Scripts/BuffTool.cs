﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BuffTool : Buff
{
    public Buff tool;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void DoEffect()
    {
        PlayerBuffManager playerBM = owner.GetComponent<PlayerBuffManager>();
        
        playerBM.SetTool(tool);
        playerBM.hasTool = true;
    }
}
