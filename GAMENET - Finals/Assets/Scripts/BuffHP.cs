﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BuffHP : Buff
{
    // Start is called before the first frame update
    void Start()
    {
        this.amount = 1;
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void DoEffect()
    {
        PlayerHealth ownerHP = owner.GetComponent<PlayerHealth>();
        
        if (PhotonNetwork.IsMasterClient)
        {
            ownerHP.photonView.RPC("AddHP", RpcTarget.AllBuffered, this.amount);
        }
    }
}
