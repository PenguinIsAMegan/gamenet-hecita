﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerData : MonoBehaviourPunCallbacks
{
    public string playerName { get; set; }
    public int score { get; set; }
    public int actorNum { get; set; }

    void Start()
    {
        score = 0;
    }

    public void addScore()
    {
        score += 100;
        object[] playerDataArray = new object[] { actorNum, score, playerName };
        LastManStandingGameManager.Instance.photonView.RPC("UpdateScore", RpcTarget.AllBuffered, playerDataArray as object);
    }
}
