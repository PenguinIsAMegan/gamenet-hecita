﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ToolTrap : Buff
{
    // Start is called before the first frame update
    void Start()
    {
        this.buffDuration = 5f;
        this.amount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void DoEffect()
    {
        object[] dataArray = new object[] { buffDuration, amount };

        owner.GetComponent<PhotonView>().RPC("TriggerToolEffect", RpcTarget.AllBuffered, dataArray as object);
    }
}
