﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class BuffInvincible : Buff
{
    // Start is called before the first frame update
    void Start()
    {
        this.buffDuration = 10f;
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void DoEffect()
    {
        owner.GetComponent<PhotonView>().RPC("TriggerInvincibleBuff", RpcTarget.AllBuffered, buffDuration);
    }
}
