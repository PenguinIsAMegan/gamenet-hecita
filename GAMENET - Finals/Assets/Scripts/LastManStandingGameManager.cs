﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class LastManStandingGameManager : MonoBehaviourPunCallbacks
{
    public Transform[] startingPositions;
    public Text[] playerNamesUI;
    public Text[] scoreUIs;
    public List<GameObject> playerList = new List<GameObject>();

    public GameObject playerPrefab;
    public GameObject winScreen;
    public Text winText;

    public static LastManStandingGameManager Instance = null;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        winScreen.SetActive(false);

        if (PhotonNetwork.IsConnectedAndReady)
        {
            int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;

            Vector3 spawnPointPosition = startingPositions[actorNumber - 1].position;
            GameObject player = PhotonNetwork.Instantiate(playerPrefab.name, spawnPointPosition, Quaternion.identity);

            playerList.Add(player);

            string playerName = player.GetComponent<PhotonView>().Owner.NickName;
            string playerScore = player.GetComponent<PlayerData>().score.ToString();

            if (player.GetComponent<PlayerData>().isActiveAndEnabled)
            {
                player.GetComponent<PlayerData>().actorNum = actorNumber;
                player.GetComponent<PlayerData>().playerName = playerName;
            }

            object[] dataArray = new object[] { actorNumber, playerName, playerScore };
            
            this.photonView.RPC("displayNameAndScore", RpcTarget.All, dataArray as object);

           if (PhotonNetwork.IsMasterClient)
           {
                SpawnStartingEnemy();
           }
        }
    }

    [PunRPC]
    public void displayNameAndScore(object[] data)
    {
        int number = (int)data[0];
        string name = (string)data[1];
        string score = (string)data[2];

        playerNamesUI[number - 1].text = name;
        scoreUIs[number - 1].text = score;
    }

    [PunRPC]
    public void UpdateScore(object[] data)
    {
        int number = (int)data[0];
        int score = (int)data[1];
        string name = (string)data[2];

        if (number > 0)
        {
            scoreUIs[number - 1].text = score.ToString();
        }
    }

    void SpawnStartingEnemy()
    {
        Spawner.Instance.canSpawn = true;
        Spawner.Instance.SpawnSlime();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActivateWinScreen()
    {
        winScreen.SetActive(true);
    }
}
