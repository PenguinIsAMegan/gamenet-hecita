﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class PlayerHealth : MonoBehaviourPunCallbacks
{
    public int startingHP = 5;
    public bool isPlayerAlive = true;
    public Text winText;

    private int currentHP;

    [SerializeField]
    TextMeshProUGUI hpUI;

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)EventCodes.RaiseEventCode.WinEventCode)
        {
            LastManStandingGameManager.Instance.ActivateWinScreen();
            Spawner.Instance.canSpawn = false;

            object[] data = (object[])photonEvent.CustomData;

            string playerWon = (string)data[0];

            winText = GameObject.Find("WinText").GetComponent<Text>();
            winText.text = playerWon + " WON!";

            Time.timeScale = 0;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHP = startingHP;
        displayHealth();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [PunRPC]
    public void TakeDamage(int amount)
    {
        this.currentHP -= amount;
        displayHealth();

        if (this.currentHP <= 0)
        {
            this.currentHP = 0;
            isPlayerAlive = false;
            photonView.RPC("CheckRemainingPlayer", RpcTarget.All);
        }
    }
    
    void displayHealth()
    {
        hpUI.text = currentHP.ToString();
    }

    IEnumerator InvisibleCountdown()
    {
        this.GetComponent<BoxCollider2D>().enabled = false;

        yield return new WaitForSeconds(3.0f);

        this.GetComponent<BoxCollider2D>().enabled = true;
    }

    [PunRPC]
    public void AddHP(int amount)
    {
        this.currentHP += amount;
        displayHealth();
    }

    [PunRPC]
    void CheckRemainingPlayer()
    {
        foreach (GameObject player in LastManStandingGameManager.Instance.playerList)
        {
            if (player.GetComponent<PlayerHealth>().isPlayerAlive)
            {
                TriggerWinEvent(player.GetComponent<PhotonView>().Owner.NickName);
            }
        }
    }


    public void TriggerWinEvent(string winnerName )
    {
        object[] data = new object[] { winnerName };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)EventCodes.RaiseEventCode.WinEventCode, data, raiseEventOptions, sendOption);
    }
}
