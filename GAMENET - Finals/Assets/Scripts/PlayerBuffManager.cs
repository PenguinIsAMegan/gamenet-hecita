﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class PlayerBuffManager : MonoBehaviourPunCallbacks
{
    public GameObject toolSP;
    public Buff tool;

    public bool hasTool = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && hasTool)
        {
            photonView.RPC("PlaceTool", RpcTarget.AllBuffered);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Buff"))
        {
            collision.collider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            Buff buffTaken = collision.collider.gameObject.GetComponent<Buff>();
            buffTaken.owner = this.gameObject;
            buffTaken.DoEffect();
            collision.collider.gameObject.GetComponent<Buff>().destroyBuffPickUp();
        }
    }

    [PunRPC]
    public void TriggerInvincibleBuff(float duration)
    {
        SpriteRenderer playerSprite = this.GetComponent<SpriteRenderer>();
        playerSprite.color = Constants.COLOR_TRANSPARENT;
        this.GetComponent<BoxCollider2D>().enabled = false;

        StartCoroutine(InvincibilityCountdown(playerSprite, duration));
    }

    IEnumerator InvincibilityCountdown(SpriteRenderer playerSprite, float duration)
    {
        yield return new WaitForSeconds(duration);

        playerSprite.color = Constants.COLOR_OPAQUE;
        this.GetComponent<BoxCollider2D>().enabled = true;
    }

    [PunRPC]
    public void TriggerToolEffect(object[] dataArray)
    {
        float duration = (float)dataArray[0];
        int amount = (int)dataArray[1];

        this.GetComponent<PlayerInput>().ChangeSpeed(amount);

        StartCoroutine(ToolDurationCountdown(duration));
    }

    IEnumerator ToolDurationCountdown(float duration)
    {
        yield return new WaitForSeconds(duration);

        this.GetComponent<PlayerInput>().ResetSpeed();
    }

    public void SetTool(Buff toolGiven)
    {
        this.tool = toolGiven;
        if (photonView.IsMine)
        {
            toolSP.GetComponent<SpriteRenderer>().sprite = this.tool.GetComponent<SpriteRenderer>().sprite;
        }
    }

    [PunRPC]
    public void PlaceTool()
    {
        Buff toolSpawned = Instantiate(this.tool, toolSP.transform.position, Quaternion.identity);
        toolSpawned.GetComponent<BoxCollider2D>().enabled = true;
        toolSP.GetComponent<SpriteRenderer>().sprite = null;

        this.hasTool = false;
    }
}
