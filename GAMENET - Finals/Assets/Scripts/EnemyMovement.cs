﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class EnemyMovement : MonoBehaviourPunCallbacks
{
    private float speed = 2.5f;
    private Animator animator;

    public bool enemyMovementEnabled;
    public bool isKilledByPlayer = false;

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)EventCodes.RaiseEventCode.EnemyKilledEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            int targetID = (int)data[0];

            if (this.photonView.ViewID == targetID)
            {
                if (this.isKilledByPlayer)
                {
                    Spawner.Instance.SpawnPickUp(this.transform.position);
                }

                Destroy(this.gameObject);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
        enemyMovementEnabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyMovementEnabled)
        {
            findClosestPlayer();
        }
    }

    void findClosestPlayer()
    {
        float distanceToClosestPlayer = Mathf.Infinity;
        GameObject closestPlayer = null;
        GameObject[] allPlayers = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject currentPlayer in allPlayers)
        {
            float distanceToCurrentPlayer = (currentPlayer.transform.position - this.transform.position).sqrMagnitude;
            
            if (distanceToCurrentPlayer < distanceToClosestPlayer)
            {
                distanceToClosestPlayer = distanceToCurrentPlayer;
                closestPlayer = currentPlayer;
            }
        }

        moveToClosestPlayer(closestPlayer);
    }

    void moveToClosestPlayer(GameObject target)
    {
        if (target.transform.position.x < this.transform.position.x)
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
        }

        else
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        animator.SetBool("isWalking", true);
        this.transform.position = Vector3.MoveTowards(this.transform.position, target.transform.position, speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            this.GetComponent<BoxCollider2D>().enabled = false;
            PlayerHealth targetHealth = collision.gameObject.GetComponent<PlayerHealth>();
            targetHealth.photonView.RPC("TakeDamage", RpcTarget.AllBuffered, 1);
            destroyEnemy();
        }
    }

    public void destroyEnemy()
    {
        int ID = this.photonView.ViewID;

        object[] data = new object[] { ID };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };
        
        PhotonNetwork.RaiseEvent((byte)EventCodes.RaiseEventCode.EnemyKilledEventCode, data, raiseEventOptions, sendOption);
    }
}
