﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Buff : MonoBehaviourPunCallbacks
{
    public GameObject owner;
    public float buffDuration;
    public int amount;
    public int toolChoice;
   

    public void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    public void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)EventCodes.RaiseEventCode.DestroyBuffPickUpEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            int targetID = (int)data[0];
            if (this.gameObject.GetInstanceID() == targetID)
            {
                Destroy(this.gameObject);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.CompareTag("Player") && !collision.collider.CompareTag("Projectile"))
        {
            destroyBuffPickUp();
        }
    }

    // Start is called before the first frame update
    protected void Start()
    {
        StartCoroutine(PickUpDuration());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void DoEffect()
    {

    }

    public void destroyBuffPickUp()
    {
        int ID = this.gameObject.GetInstanceID();

        object[] data = new object[] { ID };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)EventCodes.RaiseEventCode.DestroyBuffPickUpEventCode, data, raiseEventOptions, sendOption);
    }

    IEnumerator PickUpDuration()
    {
        yield return new WaitForSeconds(5f);
        destroyBuffPickUp();
    }
}
