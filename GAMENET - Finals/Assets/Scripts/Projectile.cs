﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Projectile : MonoBehaviourPunCallbacks
{
    private float speed = 20;
    private Rigidbody2D r2d;
    private bool isDestroyable;
    public Vector3 direction;

    public GameObject owner;

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)EventCodes.RaiseEventCode.DestroyProjectileObjectEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            
            bool isTargetDestroyable = (bool)data[0];
            int targetID = (int)data[1];

            if (isTargetDestroyable == true && this.gameObject.GetInstanceID() == targetID)
            {
                Destroy(this.gameObject);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<BoxCollider2D>().enabled = false;
        r2d = this.GetComponent<Rigidbody2D>();
        
        StartCoroutine(EnableCollider());
    }

    // Update is called once per frame
    void LateUpdate()
    {
       if (direction != null)
       {
           r2d.velocity = direction * speed;
       }
    }

    IEnumerator EnableCollider()
    {
        yield return new WaitForSeconds(0.1f);

        this.GetComponent<BoxCollider2D>().enabled = true;
        this.isDestroyable = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            owner.GetComponent<PlayerData>().addScore();
            collision.gameObject.GetComponent<EnemyMovement>().isKilledByPlayer = true;
            collision.gameObject.GetComponent<EnemyMovement>().destroyEnemy();
            this.isDestroyable = true;
            destroyProjectile();
        }

        else if (collision.collider.CompareTag("Player") && collision.collider.gameObject != owner.gameObject)
        {
            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("lsm"))
            {
                this.isDestroyable = true;
                destroyProjectile();
            }
        }

        else if (collision.collider.CompareTag("Environment") || collision.collider.CompareTag("Buff"))
        {
            this.isDestroyable = true;
            destroyProjectile();
        }
    }

    public void destroyProjectile()
    {
        bool isProjectileDestroyable = this.isDestroyable;
        int ID = this.gameObject.GetInstanceID();

        object[] data = new object[] { isProjectileDestroyable, ID };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)EventCodes.RaiseEventCode.DestroyProjectileObjectEventCode, data, raiseEventOptions, sendOption);
    }
}
