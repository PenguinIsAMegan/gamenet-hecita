﻿using UnityEngine;

public class Constants
{
    public const string PLAYER_READY = "isPlayerReady";
    public const string PLAYER_SELECTION_NUMBER = "playerSelectionNumber";
    public static Color COLOR_TRANSPARENT = new Color(1f, 1f, 1f, .5f);
    public static Color COLOR_OPAQUE = new Color(1f, 1f, 1f, 1f);
}
