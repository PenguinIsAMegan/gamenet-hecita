﻿public static class EventCodes
{
    public enum RaiseEventCode
    {
        DestroyProjectileObjectEventCode = 0,
        EnemyKilledEventCode = 1,
        DestroyBuffPickUpEventCode = 2,
        SpawnPickUpEventCode = 3,
        WinEventCode = 4
    }
}

