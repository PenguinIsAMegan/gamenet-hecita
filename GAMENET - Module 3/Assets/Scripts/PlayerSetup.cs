﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;

    public float maxHP = 100;
    private float currentHP;

    public Image healthBar;

    [SerializeField]
    TextMeshProUGUI playerNameText;

    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
        }

        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            //GetComponent<VehicleHealth>().enabled = true; // photonView.IsMine;
            GetComponent<VehicleShooting>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;

            //currentHP = maxHP;
            //healthBar.fillAmount = currentHP / maxHP;
        }

        playerNameText.text = photonView.Owner.NickName;
    }


}
