﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Projectile : MonoBehaviourPunCallbacks
{
    public float speed;
    public int damage;

    public GameObject direction;
    
    public GameObject owner;

    private Rigidbody rdbd;

    private float timer = 0f;
    private float lifespan = 5f;

    // Start is called before the first frame update
    void Start()
    {
        rdbd = GetComponent<Rigidbody>();
        speed = 25;
        damage = 25;

        if (direction != null)
        {
            this.transform.rotation = direction.transform.rotation;
        }

        if (owner != null)
        {
            Debug.Log("Owner: " + this.owner.GetComponent<PhotonView>().Owner.NickName);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Owner: " + this.owner);
        Debug.Log("Damage: " + this.damage);
        timer += Time.deltaTime;
        
        if (timer >= lifespan)
        {
            Destroy(this.gameObject);
        }
    }

    void LateUpdate()
    {
        if (direction != null)
        {
            //transform.position += Time.deltaTime * speed * direction.transform.forward;
            
            rdbd.velocity = gameObject.transform.forward * speed;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            Debug.Log("Player hit!");
            VehicleHealth targetHealth = collision.gameObject.GetComponent<VehicleHealth>();
            //collision.gameObject.GetComponent<VehicleHealth>().TakeDamage(damage);
            //collision.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
            //targetHealth.SetKiller(owner);
            targetHealth.photonView.RPC("SetKiller", RpcTarget.AllBuffered, owner.GetComponent<PhotonView>().ViewID);
            targetHealth.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
            
            Destroy(this.gameObject);
        }

        else if (collision.collider != this.gameObject)
        {
            Destroy(this.gameObject);
        }
    }

    public void Test()
    {
        Debug.Log("Working");
    }
}
