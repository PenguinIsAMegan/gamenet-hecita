﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class VehicleShooting : MonoBehaviourPunCallbacks
{
    public bool isShootingEnabled;

    public GameObject projectilePrefab;
    public GameObject hitEffectPrefab;
    
    public Transform weaponSP;
    public GameObject bulletDir;

    private float timer = 10f;

    public float fireRate;
    public int laserDamage = 10;
    public int killCountCondition;
    public int killCount;

    //public Text winText;

    public enum WeaponType
    {
        MachineGun,
        Laser
    };

    //public enum RaiseEventCode
    //{
    //    AnnounceWinner = 2
    //}

    public WeaponType weaponType;

    //private void OnEnable()
    //{
    //    PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    //}

    //private void OnDisable()
    //{
    //    PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    //}

    //void OnEvent(EventData photonEvent)
    //{
    //    if (photonEvent.Code == (byte)RaiseEventCode.AnnounceWinner)
    //    {
    //        object[] data = (object[])photonEvent.CustomData;

    //        string winner = (string)data[0];

    //        winText = GameObject.Find("WinText").GetComponent<Text>();

    //        winText.text = winner + " won!";
    //    }
    //}

    // Start is called before the first frame update
    void Start()
    {
        isShootingEnabled = false;

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            this.enabled = false;
        }

        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            if (this.weaponType == WeaponType.MachineGun)
            {
                DeathRaceManager.instance.DisableCrosshair();
                this.fireRate = 0.5f;
            }

            else if (this.weaponType == WeaponType.Laser)
            {
                this.fireRate = 0.2f;
            }
        }

        killCountCondition = PhotonNetwork.CurrentRoom.PlayerCount - 1;
        killCount = 0;
        Debug.Log("killCountCondition: " + killCountCondition);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Kills: " + this.killCount);

        if (isShootingEnabled && photonView.IsMine)
        {
            if (this.timer < this.fireRate)
            {
                this.timer += Time.deltaTime;
            }

            if (Input.GetButton("Fire1") && this.timer > this.fireRate)
            {
                this.timer = 0f;

                this.weaponSP = transform.Find("WeaponSP").GetComponent<Transform>();
                
                if (this.weaponType == WeaponType.MachineGun)
                {
                    this.bulletDir = GameObject.Find("BulletDirection");
                    GameObject Projectile = PhotonNetwork.Instantiate(projectilePrefab.name, this.weaponSP.position, Quaternion.identity);
                    Projectile.GetComponent<Projectile>().direction = this.bulletDir;
                    Projectile.GetComponent<Projectile>().owner = this.gameObject; // this.photonView.Owner.NickName;
                }

                else if (this.weaponType == WeaponType.Laser)
                {
                    RaycastHit hit;
                    Ray ray = this.GetComponent<PlayerSetup>().camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

                    if (Physics.Raycast(ray, out hit, 100))
                    {
                        Debug.Log(hit.collider.gameObject.name);
                       
                        photonView.RPC("CreateHitEffect", RpcTarget.All, hit.point);

                        if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                        {
                            Debug.Log("Laser Hit!");
                            VehicleHealth targetHealth = hit.collider.gameObject.GetComponent<VehicleHealth>();
                            //targetHealth.SetKiller(this.gameObject);
                            targetHealth.photonView.RPC("SetKiller", RpcTarget.All, this.GetComponent<PhotonView>().ViewID);
                            targetHealth.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, laserDamage);
                        }
                    }
                }
               
            }
        }
    }

    [PunRPC]
    public void CreateHitEffect(Vector3 position)
    {
        GameObject hitEffect = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffect, 0.2f);
    }

    //public void AddKillCount()
    //{
    //    this.killCount++;

    //    if (this.killCount >= killCountCondition)
    //    {
    //        AnnounceWinner(this.GetComponent<PhotonView>().Owner.NickName);
    //    }
    //}

    //public void AnnounceWinner(string winner)
    //{
    //    DeathRaceManager.instance.ActivateWinScreen();

    //    string lastPlayer = winner;

    //    object[] data = new object[] { lastPlayer };

    //    RaiseEventOptions raiseEventOptions = new RaiseEventOptions
    //    {
    //        Receivers = ReceiverGroup.All,
    //        CachingOption = EventCaching.AddToRoomCache
    //    };

    //    SendOptions sendOption = new SendOptions
    //    {
    //        Reliability = false
    //    };

    //    PhotonNetwork.RaiseEvent((byte)RaiseEventCode.AnnounceWinner, data, raiseEventOptions, sendOption);
    //}
}
