﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class VehicleHealth : MonoBehaviourPunCallbacks
{
    public float maxHP = 100;
    
    private float currentHP;
    //private string eliminatedPlayer;

    public Image healthBar;

    public Text eliminatedText;
    public Text winText;
    

    public GameObject projectileOwner;

    public enum RaiseEventCode
    {
        WhoDiedEventCode = 1,
        AnnounceWinner = 2
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventCode.WhoDiedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string eliminatedPlayer = (string)data[0];
            int viewID = (int)data[1];
            string killerPlayer = (string)data[2];

            eliminatedText = GameObject.Find("EliminatedText").GetComponent<Text>();

            eliminatedText.text = eliminatedPlayer + " is eliminated by " + killerPlayer +"!";
            eliminatedText.color = Color.red;

            StartCoroutine(ClearText());
        }

        if (photonEvent.Code == (byte)RaiseEventCode.AnnounceWinner)
        {
            object[] data = (object[])photonEvent.CustomData;

            string winner = (string)data[0];

            winText = GameObject.Find("WinText").GetComponent<Text>();

            winText.text = winner + " won!";
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            this.enabled = false;
        }

        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            //GameObject hpbar = GameObject.Find("HealthbarImage");
            //healthBar = hpbar.GetComponent<Image>();
            currentHP = maxHP;
            healthBar.fillAmount = currentHP / maxHP;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [PunRPC]
    public void TakeDamage(int amount)
    {
        this.currentHP -= amount;
        this.healthBar.fillAmount = currentHP / maxHP;

        Debug.Log("taken damage, currentHP: " + this.currentHP);

        if (this.currentHP <= 0)
        {
            this.GetComponent<VehicleMovement>().enabled = false;
            this.GetComponent<VehicleShooting>().isShootingEnabled = false;

            //eliminatedPlayer = this.gameObject.GetComponent<PhotonView>().Owner.NickName;
            //AnnounceEliminatedPlayer(info.Sender.NickName);
            AnnounceEliminatedPlayer(projectileOwner.GetComponent<PhotonView>().Owner.NickName);

            VehicleShooting killerStat = projectileOwner.GetComponent<VehicleShooting>();
            killerStat.killCount++;

            if (killerStat.killCount >= killerStat.killCountCondition)
            {
                AnnounceWinner(projectileOwner.GetComponent<PhotonView>().Owner.NickName);
            }
            
        }
    }

    public void AnnounceEliminatedPlayer(string killer)
    {
        string eliminatedPlayer = photonView.Owner.NickName;
        int viewID = photonView.ViewID;
        string killerPlayer = killer;

        object[] data = new object[] { eliminatedPlayer, viewID, killer };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.WhoDiedEventCode, data, raiseEventOptions, sendOption);
    }

    [PunRPC]
    public void SetKiller(int killerID)
    {
        //this.projectileOwner = killerObject;

        this.projectileOwner = PhotonView.Find(killerID).gameObject;
    }

    public void AnnounceWinner(string winner)
    {
        DeathRaceManager.instance.ActivateWinScreen();

        string lastPlayer = winner;

        object[] data = new object[] { lastPlayer };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.AnnounceWinner, data, raiseEventOptions, sendOption);
    }

    IEnumerator ClearText()
    {
        yield return new WaitForSeconds(3f);
        eliminatedText.text = " ";
    }
}
